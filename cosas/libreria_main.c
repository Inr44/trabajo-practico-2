#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "libreria_main.h"

/*
representamos numeros como ints
generar_numero_aleatorio: int -> int
Recibe un numero entero, y devuelve un numero aleatorio positivo entre 0 y ese numero (o su opuesto
en el caso de que el numero recibido como argumento sea negativo)
Dado que es una funcion aleatoria no se proporcionan ejemplos, ya que no produce ningun resultado certero.
*/

int generar_numero_aleatorio(int maximo)
{
	return rand() % maximo;
}

/*
representamos palabras o frases como cadenas de caracteres
invertir: char[] char[]
Recibe dos cadenas de caracteres. Copia el contenido de la primera
en la segunda, invirtiendo el orden de todos los caracteres.
Entrada: ['a','m','o','r','\0'], [] Salida: ['a','m','o','r','\0'] , ['r','o','m','a','\0']
Entrada: ['c','a','s','a',' ','l','i','n','d','a','\0'], []
Salida: ['c','a','s','a',' ','l','i','n','d','a','\0'] ['a','d','n','i','l',' ','a','s','a','c','\0']
Aclaraciones: El programa no tiene salida por ser una funcion void, pero los ejemplos
muestran como se alteran las cadenas que se reciben como parametros despues de la ejecucion de la funcion.
El contenido de la cadena 'destino' es antes de la ejecucion, por eso esta vacio en el ejemplo.
*/

void invertir(char fuente[], char destino[]){
	int largo, i = 0;

	for (largo = strlen(fuente)-1; largo >= 0; largo--, i++){
		destino[i] = fuente[largo];
	}

	destino[i] = '\0';

}

/*
representamos palabras o frases como cadenas de caracteres
LowerString: char[] char[]
Recibe dos cadenas de caracteres. Copia el contenido de la primera
en la segunda, convirtiendo todas las letras en minuscula.
Entrada: ['A','M','O','R','\0'], [] Salida: ['A','M','O','R','\0'] , ['a','m','o','r','\0']
*/

void LowerString(char fuente[], char destino[]){
	int i;

	for(i = 0; fuente[i]; i++)
		destino[i] = tolower(fuente[i]);

    destino[i] = '\0';
}

/*
representamos palabras o frases como cadenas de caracteres, largos de palabras como int y valores booleanos como int
Substring: char[], char[], int, int -> int
Recibe dos cadenas y sus respectivos largos. Revisa si la segunda cadena se encuentra adentro de la primera.
Devuelve 1 en caso de encontrarla, 0 en caso contrario (0 representando 'False' y 1 representando 'True').
Comienza  comparando los largos, en caso de que la primer cadena sea mas corta que la segunda es imposible
que esta se encuentre adentro de la primera, por lo tanto devuelve 0.
Luego calcula la diferencia entre el largo de las dos palabras. Despues de esto comienza una iteracion
que copia los primeros n caracteres a una cadena auxiliar, siendo n el largo de la segunda cadena, y compara que dichas
cadenas sean iguales, en cuyo caso termina la iteracion, y devuelve 1.
Posteriormente a la comparacion, se le suma 1 al puntero que apunta a la primer palabra.
Esto funciona porque un array es efectivamente un puntero al primer elemento del mismo, y al sumarle uno,
lo estoy desplazando a la direccion de memoria inmediatamente siguiente, que contiene al segundo elemento
del array. Hacer eso se puede interpretar como decir que el array ahora comienza en lo que antes era su segundo
elemento, y todas los elementos del array se movieron un lugar para atras (pero no cambiaron su ubicacion en memoria).
Suponiendo que antes la comparacion con la cadena auxiliar no devolvio verdadero, se repite la comparacion, pero
ahora se copia la primera cadena desde su nuevo comienzo, el cual es el elemento que previamente estaba segundo.
Vale destacar que la funcion es sensible a la capitalizacion de ambas cadenas, esto es intencional.
Se podria hacer que la funcion calcule los largos de las palabras para que sea mas autonoma, pero no se
hace de esta forma para que el programa sea mas eficiente (los largos se calculan una sola vez en otra funcion, y se evita recalcularlos cada vez
que se llama a esta funcion)
 Entrada: ("Murcielago", "lago", 10, 4) Salida: 1
 Entrada: ("Murcielago", "Lago", 10, 4) Salida: 0
 Entrada: ("Navegacion", "Nave", 10, 4) Salida: 1
 Entrada: ("Navegacion", "nave", 10, 4) Salida: 0
 */

int Substrings(char Palabra1[], char Palabra2[], int Largo_Pal1, int Largo_Pal2){
	int Salida = 0, Diferencia_largo, i;
	char Cadena_aux_Comparaciones[15];

	if (Largo_Pal1 < Largo_Pal2)
		return Salida;

	else {
		Diferencia_largo = Largo_Pal1 - Largo_Pal2+1;
		for(i = 0; i < Diferencia_largo && Salida == 0; i++){

			strncpy(Cadena_aux_Comparaciones, Palabra1, Largo_Pal2);
			Cadena_aux_Comparaciones[Largo_Pal2]='\0';

			if(strcmp (Cadena_aux_Comparaciones,Palabra2) == 0)
				Salida = 1;
			Palabra1++;
		}

	}

	return Salida;
}

/*
Representamos palabras o frases como cadenas de caracteres, listas de palabras como array de cadenas de caracteres, largos de palabras como int, cantidad de palabras como int y valores booleanos como int
validar_palabra: char[], char[][], int, int[] -> int
Recibe una cadena, una lista de cadenas, la cantidad de palabras que hay en dicha lista de cadenas, y una lista de largos.
Devuelve 1 en caso de que la palabra cumpla ciertas condiciones, 0 en caso contrario.
Las condiciones son:
-Tener un largo mayor a 3
-No ser una substring de alguna de las palabras en la lista de cadenas.
-No contener como substring a alguna de las palabras en la lista de cadenas.
-No ser el reverso de una de las palabras de la lista de cadenas.
Si la palabra cumple las condiciones, guarda su largo en la ultima posicion del array de largos, y la copia
en el array de palabras.
*/
int validar_palabra(char palabra_actual[], char palabras[][15], int palabras_puestas, int largos[]){
	int largo, i, salida = 1;
	char inversa[15], minuscula[15];

	largo = strlen(palabra_actual);

	if (largo < 4)
		return 0;

	LowerString(palabra_actual, minuscula);


	if (!palabras_puestas){
		largos[0] = largo;
		strcpy(palabras[0], minuscula);
		return 1;
	}
	else{
		invertir(palabra_actual, inversa);

		for(i = 0; i < palabras_puestas && salida == 1; i++){
			if(Substrings(palabras[i], minuscula, largos[i], largo) ||
				 Substrings(minuscula, palabras[i], largo, largos[i]) ||
				 strcmp(palabras[i], inversa) == 0)
					salida = 0;
			}
	}

	if (salida){
		largos[i] = largo;
		strcpy(palabras[i], minuscula);
	}


	return salida;
}
/*
Representamos listas de palabras como arrays de cadenas de caracteres, y cantidades de palabras como int.
elegir_palabras: char[][], char[][], int, int -> int
Recibe dos array de cadenas de caracteres, la cantidad de palabras que se encuentran en el primer array, y la cantidad de palabras
que deben copiarse al segundo array. Copia aleatoriamente palabras del primer array al segundo array, siempre y cuando
dichas palabras respeten ciertas condiciones (ver validar_palabras), hasta alcanzar la cantidad de palabras
especificadas, o intentar copiar todas las palabras del primer array, lo que suceda primero. Devuelve la
cantidad de palabras que copio.
Para evitar revisar la misma palabra dos veces, se crea un array de enteros que contiene todos los indices
no revisados, asi como un int que representa la cantidad de indices sin revisar. Luego se genera un numero aleatorio
entre 0 y la cantidad de indices, y se toma el indice que se encuentra en la posicion del array asociada al
numero generado aleatoriamente. Luego se copia en dicha posicion el ultimo elemento del array, y se disminuye
en 1 la cantidad de indices restantes. Con el ultimo elemento del array se refiere al array en la posicion indicada
por los indices restantes, todos los elementos que estan luego de esa posicion se ignoran.
Una vez elegida la palabra aleatoria, se revisa si es valida, y de ser asi, se copia al segundo array de palabras,
y se suma uno al contador que lleva registro de la cantidad de palabras copiadas. En caso contrario no se hace nada.
Esto se repite hasta haber encontrado la cantidad de palabras solicitadas, o agotar las opciones de palabras.
A fines de eficiencia se utiliza un array de enteros que almacena los largos de todas las palabras colocadas,
para evitar recalcular el largo de cada una de las palabras cada vez que se necesita comparar su largo, y
un array de cadenas de caracteres, donde se guarda una copia de todas las palabras, pero con todas sus letras
en minuscula, para poder revisar si una es substring de otra independientemente de la capitalizacion, y sin
tener que volver a convertirla en cada comparacion.
*/
int elegir_palabras(char palabras[][15], char palabras_seleccionadas[][15], int total_palabras, int cantidad_palabras_a_elegir)
{
	int indices_posibles[100], largos[100], indices_restantes = total_palabras, indice_elegido, palabra_a_revisar,
		contador_palabras = 0, i;
	char palabras_minusculas[100][15];


	for (i = 0; i < total_palabras; i++){
		indices_posibles[i] = i;
	}

	while (contador_palabras < cantidad_palabras_a_elegir && indices_restantes > 0){

		indice_elegido = generar_numero_aleatorio(indices_restantes);
		palabra_a_revisar = indices_posibles[indice_elegido];
		indices_posibles[indice_elegido] = indices_posibles[indices_restantes-1];
		indices_restantes--;

		if (validar_palabra(palabras[palabra_a_revisar], palabras_minusculas, contador_palabras, largos)){
			strcpy(palabras_seleccionadas[contador_palabras], palabras[palabra_a_revisar]);
			contador_palabras++;
		}
	}

	return contador_palabras;

}

/*
Representamos nombres de archivos como cadenas de caracteres, y lista de palabras como array de cadenas de caracteres
Lectura_Archivo_Palabras: char[], char[][] -> int
Recibe el nombre de un archivo, y un array de cadenas de caracteres. El archivo debe contener una lista de palabras, una
por renglon, y la funcion copia las primeras 100 al array, poniendo una palabra en cada cadena de caracteres.
Devuelve la cantidad de palabras que copio al array.
*/
int Lectura_Archivo_Palabras(char Archivo_Entrada[], char Lista_De_Palabras[][15]){

	FILE *Puntero_Archivo = fopen(Archivo_Entrada, "r");
	int Contador_Palabras = 0;
	char buff[15];

	while(!feof(Puntero_Archivo) && Contador_Palabras < 100){
		fscanf(Puntero_Archivo, "%s", buff);
		strcpy(Lista_De_Palabras[Contador_Palabras],buff);
		if (!feof(Puntero_Archivo))
			Contador_Palabras++;
	}
	fclose(Puntero_Archivo);

	return Contador_Palabras;

}
/*
Representamos nombres de archivos como cadenas de caracteres, lista de palabras como array de cadenas de caracteres y una cantidad de palabras como int
Lectura_Archivo_Palabras: char[], char[][], int
Recibe el nombre de un archivo, una lista de palabras, y una cantidad de palabras.
Escribe en el archivo las palabras de la lista de palabras, escribiendo una por renglon. En caso de no existir
el archivo lo crea. El archivo termina en un renglon en blanco.
*/
void Escritura_Archivo_Palabras(char Archivo_Escritura[], char Lista_De_Palabras[][15], int cantidad_palabras){

	FILE *Puntero_Archivo = fopen(Archivo_Escritura, "w");
	int Contador_Palabras = 0;
	char buff[15];

	while(Contador_Palabras < cantidad_palabras){

		strcpy(buff,Lista_De_Palabras[Contador_Palabras]);
		fprintf(Puntero_Archivo,"%s\n" , buff);

		Contador_Palabras++;
	}
	fclose(Puntero_Archivo);
}


/*
Representamos palabras como cadenas de caracteres.
menu: char[], char[] -> int
Recibe dos cadenas de caracteres. Le solicita al usuario por consola que ingrese los nombres de los archivos 
de entrada y de salida, y guarda los nombres en las cadenas que recibe como parametros. Se hace una validacion de 
la entrada y la salida antes de finalizar la funcion, y en caso de que sean invalidas se le solicita al usuario
intentar nuevamente, o, en el caso de la entrada, se le da la opcion de finalizar el programa.
La validacion consiste en revisar que lo ingresado no sea solo un enter, y en el caso de la entrada, que el archivo
de entrada efectivamente exista.
*/
int menu(char entrada[], char salida[]){

	int output = -1;
	char respuesta[10];
	FILE *Puntero_Archivo;


	do {
        printf("Ingrese el nombre del archivo de entrada:\n");
		fgets(entrada, 100, stdin);
		if (entrada[0] == '\n')
			printf("Tiene que ingresar un archivo de entrada, intente nuevamente.\n");

       else{
			entrada[strlen(entrada)-1] = '\0';
			Puntero_Archivo = fopen(entrada, "r");

			if(Puntero_Archivo){
				fclose(Puntero_Archivo);
                output = 1;
			}else{

				do{
					printf("%s\n%s\n%s\n",
						"El archivo ingresado no existe.",
						"Desea intentar nuevamente? (En caso de no hacerlo finalizara el programa)",
						"Respuestas validas: \'si\' o \'no\'");
					scanf("%9s%*c", respuesta);
				}while(strcmp(respuesta, "si") && strcmp(respuesta, "no"));

				if (!strcmp(respuesta, "no"))
					output = 0;
			}

		}
	} while(entrada[0] == '\n' || output == -1);

	if (!output)
		return 0;


	printf("Ingrese el nombre del archivo de salida:\n");

	do {
		fgets(salida, 100, stdin);
		if (salida[0] == '\n')
			printf("Tiene que ingresar un archivo de salida, intente nuevamente.\n");
	} while(salida[0] == '\n');

	salida[strlen(salida)-1] = '\0';


	return output;

}
