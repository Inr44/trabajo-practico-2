#ifndef _libreria_main_h
#define _libreria_main_h

int generar_numero_aleatorio(int maximo);

void invertir(char fuente[], char destino[]);

void LowerString(char fuente[], char destino[]);

int Substrings(char Palabra1[], char Palabra2[], int Largo_Pal1, int Largo_Pal2);

int validar_palabra(char palabra_actual[], char palabras[][15], int palabras_puestas, int largos[]);

int elegir_palabras(char palabras[][15], char palabras_seleccionadas[][15], int total_palabras, int cantidad_palabras_a_elegir);

int Lectura_Archivo_Palabras(char Archivo_Entrada[], char Lista_De_Palabras[][15]);

void Escritura_Archivo_Palabras(char Archivo_Escritura[], char Lista_De_Palabras[][15], int cantidad_palabras);

int menu(char entrada[], char salida[]);

#endif // MAIN2_H_INCLUDED
